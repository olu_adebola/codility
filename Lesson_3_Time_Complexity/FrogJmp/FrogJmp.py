def solution(X, Y, D):
    # write your code in Python 3.6
    min_jumps = int((Y - X)/ (D))
    if ((Y - X) % D == 0): #if (y-x)/d yields no remainder return ...
        return min_jumps
    else:
        return int((Y - X)// (D)) + 1  # in other cases round down min_jumps and add 1
