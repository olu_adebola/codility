def solution(A):
    # write your code in Python 3.6
    N = len(A)
    A_Tot = int(((N + 1)*(N + 2))/2) # total value(n(n+1)/2), since array has N + 1 variables
    for i in range(N):
        A_Tot -= A[i] # subtracting the values in array from total so we're left with missing value
    return A_Tot    
