# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6
    """
    What i plan to do here is to first find the total sum of the array
    why? because my plan is to append the absolute value of the difference between the sum
    of the current and previous values(temporary total) from the total sum.
    then i'll return minimum value from new list B
    """
    total_A = sum(A) #The total sum of N values in A
    #print(total_A)
    temp_tot = 0 #temporary total instiated on zero
    B = [] #my differences will be added to this empty list
    for i in range(len(A)):
        min_diff = abs(temp_tot - (total_A - temp_tot))
        #print(min_diff,i)
        B.append(abs(min_diff))#append absolute value of temporary total minus Total
        temp_tot += int(A[i])# increase the value of temporary total by new value in list A
        #print(temp_tot,i)
    return int(min(B[1:])) #returning minimum value in B, not including first value
