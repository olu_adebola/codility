def solution(N, A):
    B = [0,] * N #this step is to generate counter board as a list
    max_max = 0  #the first values start at zero
    curr_max = 0
    print(B)

    for X in A:
        if X <= N and X >= 1: #if value A[K] is smaller and equal to N and greater and equal to 1
            M = X-1 #M refers to the index of array B

            if B[M] < curr_max:
              B[M] = curr_max

            B[M] += 1 #then increase value at position A[K], which in B would be A[K] - 1

            if B[M] > max_max: #this is how i keep track of current max without using Max() later
                max_max = B[M]
        else:
            curr_max = max_max
        print(B)
    for i in range(N):
        if B[i] < curr_max:
            B[i] = curr_max
    print(B)
    return (B)

N = 5
A = [3,4,4,6,1,4,4]
import time
now = time.time()

solution(N, A)

print(time.time()-now)
#this solution still requireswork, as it needs to still be 6 times faster for extremely large ranges
