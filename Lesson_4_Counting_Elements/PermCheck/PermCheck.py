def solution(A):
    # write your code in Python 3.6
    """
    My initial thought is that this needs to be sorted.
    Then i think i need to count from 1 up as well as from the last number down.
    if numbers are consecutive from both ends, then it's a permutation, otherwise it ain't
    """
    A = sorted(A)
    mask = 1 #this is the value that will be returned, setting it to 1 assumes perm... otherwise should return 0
    current_num = 1 #permutations start at one as per definition permutations
    for num in A[0::1]:# for the number in the array when counting, by 1 from smallest to largest
        if num == current_num:
            current_num += 1
            continue    #num should equal current_num unless there is a missing value for num, if this the case, keep going
        else:
            mask = 0    #... if this not the case set mask to 0
            break
    return mask
    for num in A[-1::-1]: # for the number in the array when counting from largest to smallest, deducting 1 each time
        current_num = A[-1] #current_num set to the last value of the array
        if num == current_num:
            current_num -= 1
            continue
        else:
            mask = 0
            break
    return mask

    '''
    improvements this could have would be for the entire function to break when a missing value
    has been detected from either end. I'm not sure that is the case at the moment.
    What i was attempting to do was threading, which i have soon realized is not achieved in this manner
    when i get a chance i'll readup on that and apply
    '''
