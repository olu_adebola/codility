"""
    So i'm looking for the earliest time the frog path has been created.
    that is, there is a leaf for each position in the path
    path length is determined by X. So for x= 6. there must be a leaf in all positions, 1 to 6
    if a path is never created, the function should return -1.
"""

def solution(X, A):
    # write your code in Python 3.6
    B = set() #using a set because B will contain leaf unique positions, don't care if leaf fell in same position multiple times
    for time_fall in range(len(A)): #for time each leaf falls, as in A[2] =3 implies leaf fell position 3 at second 2
        B.add(A[time_fall]) #add each position to B
        if len(B) == X: #if there is a leaf in each position, such that X, path length is covered
            return time_fall #returns the time the last leaf fell to commplete the set
    return -1 #catch case, if the above doesn't happen then return -1        
